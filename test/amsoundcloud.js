function testUrl(client, url) {
    client
        .url(url)
        .waitForElementVisible('#app', 1000)
        //.setValue('input[type=text]', 'nightwatch')
        //.waitForElementVisible('.customImage', 1000)
        .click('button[title="Play"]')
        .pause(500000)
        //.verify.visible('.player-control.pause-state')
        .click('button[title="Pause"]')
        .pause(1000)
        .verify.visible('.listenContext')
        .click('button[title="Play"]')
        .pause(800000)
        //.verify.visible('.player-control.pause-state')
        //.assert.containsText('#main', 'Night Watch')
        .end();
}

module.exports = {
    'amsoundcloud1' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-with-guest-omax-omar-on-trax-radio-190617');
    },
    'amsoundcloud2' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-with-guest-omax-omar-on-trax-radio-190617');
    },
    'amsoundcloud3' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-with-guest-omax-omar-on-trax-radio-190617');
    },
    'amsoundcloud4' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-with-guest-omax-omar-on-trax-radio-190617');
    },
    'amsoundcloud5' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-with-guest-omax-omar-on-trax-radio-190617');
    },
    'amsoundcloud6' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-with-guest-omax-omar-on-trax-radio-190617');
    },
    'amsoundcloud7' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-with-guest-omax-omar-on-trax-radio-190617');
    },
    'amsoundcloud8' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-with-guest-omax-omar-on-trax-radio-190617');
    },
    'amsoundcloud9' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-with-guest-omax-omar-on-trax-radio-190617');
    },
    'amsoundcloud10' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-with-guest-omax-omar-on-trax-radio-190617');
    },
    'amsoundcloud11' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-with-guest-omax-omar-on-trax-radio-190617');
    },
    'amsoundcloud12' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-with-guest-omax-omar-on-trax-radio-190617');
    },
    'amsoundcloud13' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-with-guest-omax-omar-on-trax-radio-190617');
    },
    'amsoundcloud14' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-with-guest-omax-omar-on-trax-radio-190617');
    },
    'amsoundcloud15' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-with-guest-omax-omar-on-trax-radio-190617');
    },
    'amsoundcloud16' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-with-guest-omax-omar-on-trax-radio-190617');
    },
    'amsoundcloud17' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-with-guest-omax-omar-on-trax-radio-190617');
    },
    'amsoundcloud18' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-with-guest-omax-omar-on-trax-radio-190617');
    },
    'amsoundcloud19' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-with-guest-omax-omar-on-trax-radio-190617');
    },
    'amsoundcloud20' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-with-guest-omax-omar-on-trax-radio-190617');
    },
    'amsoundcloud21' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-live-trax-radio-broadcast-15062017');
    },
    'amsoundcloud22' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-live-trax-radio-broadcast-15062017');
    },
    'amsoundcloud23' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-live-trax-radio-broadcast-15062017');
    },
    'amsoundcloud24' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-live-trax-radio-broadcast-15062017');
    },
    'amsoundcloud25' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-live-trax-radio-broadcast-15062017');
    },
    'amsoundcloud26' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-live-trax-radio-broadcast-15062017');
    },
    'amsoundcloud27' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-live-trax-radio-broadcast-15062017');
    },
    'amsoundcloud28' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-live-trax-radio-broadcast-15062017');
    },
    'amsoundcloud29' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-live-trax-radio-broadcast-15062017');
    },
    'amsoundcloud30' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-live-trax-radio-broadcast-15062017');
    },
    'amsoundcloud31' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-live-trax-radio-broadcast-15062017');
    },
    'amsoundcloud32' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-live-trax-radio-broadcast-15062017');
    },
    'amsoundcloud33' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-live-trax-radio-broadcast-15062017');
    },
    'amsoundcloud34' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-live-trax-radio-broadcast-15062017');
    },
    'amsoundcloud35' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-live-trax-radio-broadcast-15062017');
    },
    'amsoundcloud36' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-live-trax-radio-broadcast-15062017');
    },
    'amsoundcloud37' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-live-trax-radio-broadcast-15062017');
    },
    'amsoundcloud38' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-live-trax-radio-broadcast-15062017');
    },
    'amsoundcloud39' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-live-trax-radio-broadcast-15062017');
    },
    'amsoundcloud40' : function (client) {
        testUrl(client, 'https://soundcloud.com/user-287039042/altered-mode-live-trax-radio-broadcast-15062017');
    },
    'amsoundcloud41' : function (client) {
        testUrl(client, 'https://soundcloud.com/alteredmode/alteredmode-technotuesdays-trax-radio-130617');
    },
    'amsoundcloud42' : function (client) {
        testUrl(client, 'https://soundcloud.com/alteredmode/alteredmode-technotuesdays-trax-radio-130617');
    },
    'amsoundcloud43' : function (client) {
        testUrl(client, 'https://soundcloud.com/alteredmode/alteredmode-technotuesdays-trax-radio-130617');
    },
    'amsoundcloud44' : function (client) {
        testUrl(client, 'https://soundcloud.com/alteredmode/alteredmode-technotuesdays-trax-radio-130617');
    },
    'amsoundcloud45' : function (client) {
        testUrl(client, 'https://soundcloud.com/alteredmode/alteredmode-technotuesdays-trax-radio-130617');
    },
    'amsoundcloud46' : function (client) {
        testUrl(client, 'https://soundcloud.com/alteredmode/alteredmode-technotuesdays-trax-radio-130617');
    },
    'amsoundcloud47' : function (client) {
        testUrl(client, 'https://soundcloud.com/alteredmode/alteredmode-technotuesdays-trax-radio-130617');
    },
    'amsoundcloud48' : function (client) {
        testUrl(client, 'https://soundcloud.com/alteredmode/alteredmode-technotuesdays-trax-radio-130617');
    },
    'amsoundcloud49' : function (client) {
        testUrl(client, 'https://soundcloud.com/alteredmode/alteredmode-technotuesdays-trax-radio-130617');
    },
    'amsoundcloud50' : function (client) {
        testUrl(client, 'https://soundcloud.com/alteredmode/alteredmode-technotuesdays-trax-radio-130617');
    },
    'amsoundcloud51' : function (client) {
        testUrl(client, 'https://soundcloud.com/alteredmode/alteredmode-technotuesdays-trax-radio-130617');
    },
    'amsoundcloud52' : function (client) {
        testUrl(client, 'https://soundcloud.com/alteredmode/alteredmode-technotuesdays-trax-radio-130617');
    },
    'amsoundcloud53' : function (client) {
        testUrl(client, 'https://soundcloud.com/alteredmode/alteredmode-technotuesdays-trax-radio-130617');
    },
    'amsoundcloud54' : function (client) {
        testUrl(client, 'https://soundcloud.com/alteredmode/alteredmode-technotuesdays-trax-radio-130617');
    },
    'amsoundcloud55' : function (client) {
        testUrl(client, 'https://soundcloud.com/alteredmode/alteredmode-technotuesdays-trax-radio-130617');
    },
    'amsoundcloud56' : function (client) {
        testUrl(client, 'https://soundcloud.com/alteredmode/alteredmode-technotuesdays-trax-radio-130617');
    },
    'amsoundcloud57' : function (client) {
        testUrl(client, 'https://soundcloud.com/alteredmode/alteredmode-technotuesdays-trax-radio-130617');
    },
    'amsoundcloud58' : function (client) {
        testUrl(client, 'https://soundcloud.com/alteredmode/alteredmode-technotuesdays-trax-radio-130617');
    },
    'amsoundcloud59' : function (client) {
        testUrl(client, 'https://soundcloud.com/alteredmode/alteredmode-technotuesdays-trax-radio-130617');
    },
    'amsoundcloud60' : function (client) {
        testUrl(client, 'https://soundcloud.com/alteredmode/alteredmode-technotuesdays-trax-radio-130617');
    },
    'amsoundcloud61' : function (client) {
        testUrl(client, 'https://soundcloud.com/alteredmode/alteredmode-technotuesdays-trax-radio-130617');
    },
    'amsoundcloud62' : function (client) {
        testUrl(client, 'https://soundcloud.com/alteredmode/alteredmode-technotuesdays-trax-radio-130617');
    },
    'amsoundcloud63' : function (client) {
        testUrl(client, 'https://soundcloud.com/alteredmode/alteredmode-technotuesdays-trax-radio-130617');
    },
    'amsoundcloud64' : function (client) {
        testUrl(client, 'https://soundcloud.com/alteredmode/alteredmode-technotuesdays-trax-radio-130617');
    },
    'amsoundcloud65' : function (client) {
        testUrl(client, 'https://soundcloud.com/alteredmode/alteredmode-technotuesdays-trax-radio-130617');
    },
    'amsoundcloud66' : function (client) {
        testUrl(client, 'https://soundcloud.com/alteredmode/alteredmode-technotuesdays-trax-radio-130617');
    },
    'amsoundcloud67' : function (client) {
        testUrl(client, 'https://soundcloud.com/alteredmode/alteredmode-technotuesdays-trax-radio-130617');
    },
    'amsoundcloud68' : function (client) {
        testUrl(client, 'https://soundcloud.com/alteredmode/alteredmode-technotuesdays-trax-radio-130617');
    },
    'amsoundcloud69' : function (client) {
        testUrl(client, 'https://soundcloud.com/alteredmode/alteredmode-technotuesdays-trax-radio-130617');
    },
    'amsoundcloud70' : function (client) {
        testUrl(client, 'https://soundcloud.com/alteredmode/alteredmode-technotuesdays-trax-radio-130617');
    },
};/**
 * Created by kirstymunro on 22/06/2017.
 */
