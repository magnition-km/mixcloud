function getRandomizer(bottom, top) {
    return function() {
        return Math.floor( Math.random() * ( 1 + top - bottom ) ) + bottom;
    }
}

var rndPause = getRandomizer( 10000, 9897000 );

var results = ""
for ( var i = 0; i<1000; i++ ) {
    results += rndPause() + " ";
}

function testUrl(client, url, setpause) {

    client
        .url(url)
        .waitForElementVisible('body', 1000)
        .waitForElementVisible('.album-art', 1000)
        .waitForElementVisible('.player-wrapper', 1000)
        .pause(5000)
        .assert.cssClassPresent('.play-button-wrap span','pause-state', 1000)
        .pause(setpause)
        .end();
}

// function playtime(client, setpause) {
//     client
//         .pause(setpause)
// }
// 45 mins 2700000
// one hour = 3600000
// two hours =  3600000 * 2
// three hours=  10700000


module.exports = {
    'ammixcloud3' : function (client) {
        testUrl(client, 'https://www.mixcloud.com/kirstymunro3/altered-mode-live-on-aatm-radio-29032018/', 3600000 * 2);
    },
    'ammixcloud31' : function (client) {
        testUrl(client, 'https://www.mixcloud.com/kirstymunro3/altered-mode-live-on-aatm-radio-29032018/', 3600000 * 2);
    },
    'ammixcloud32' : function (client) {
        testUrl(client, 'https://www.mixcloud.com/kirstymunro3/altered-mode-live-on-aatm-radio-29032018/', 3600000 * 2);
    },
    'ammixcloud33' : function (client) {
        testUrl(client, 'https://www.mixcloud.com/kirstymunro3/altered-mode-live-on-aatm-radio-29032018/', 3600000 * 2);
    },
    'ammixcloud34' : function (client) {
        testUrl(client, 'https://www.mixcloud.com/kirstymunro3/altered-mode-live-on-aatm-radio-29032018/', 3600000 * 2);
    },
    'ammixcloud35' : function (client) {
        testUrl(client, 'https://www.mixcloud.com/kirstymunro3/altered-mode-live-on-aatm-radio-29032018/', 3600000 * 2);
    },
    'ammixcloud36' : function (client) {
        testUrl(client, 'https://www.mixcloud.com/kirstymunro3/altered-mode-live-on-aatm-radio-29032018/', 3600000 * 2);
    },
    'ammixcloud3' : function (client) {
        testUrl(client, 'https://www.mixcloud.com/kirstymunro3/altered-mode-live-on-aatm-radio-29032018/', 3600000 * 2);
    }
};