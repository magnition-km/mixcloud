var followUrl = 'https://www.mixcloud.com/johndigweed/followers/';
module.exports = {
    'Get to login page': function(browser){
        browser
            .url('https://www.mixcloud.com')
            .waitForElementVisible('body', 1000)
            .waitForElementVisible('.user-actions', 1000)
            .click('.user-actions a[m-dialog="/authentication/login-modal/"]')
            .waitForElementVisible('.modal .auth-modal', 1000)
            .verify.visible('.modal .auth-modal');
    },
    'logging in': function(browser){
        browser
            .setValue('.modal .auth-form input[ng-model="formData.email"]', 'progression303@gmail.com')
            .setValue('.modal .auth-form input[ng-model="formData.password"]', 'progress51')
            .click('.modal .auth-form button[tabindex="3"]')
            .waitForElementVisible('.notifications', 1000)
            .verify.visible('.notifications')
            .url('https://www.mixcloud.com/traxradio/')
            .waitForElementVisible('.stats-box', 1000)
            .assert.visible('.stats-box')
    },
    'followusers': function(browser) {

        browser
            .url(followUrl)
            .waitForElementVisible('body', 1000)
            .waitForElementVisible('.user-actions', 1000)
            .waitForElementVisible('.player-wrapper', 1000)
            .waitForElementVisible('ul.people-to-follow', 1000)


            .elements('css selector', 'ul.people-to-follow li button',
                function (result) {
                    for (var i = 0; i < result.value.length; i++) {
                        browser.click('ul.people-to-follow li button');
                    }
                }
            )
            .pause(1000)

    }
};
